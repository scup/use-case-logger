# Use Case Logger - The Library for the incredible solution developed to log use cases on Scup Projects

### Installing
    Add to your package.json dependencies 

```javascript
"use-case-logger": "git+ssh://git@bitbucket.org:scup/use-case-logger#master"
```
    $ yarn

### Configuration

#### 
```javascript
//object
const useCaseLogger = require('use-case-logger')

const useCase = ...

const options = {
    projectIdPath: 'fields used on some cases on scup projects',
    ignoreFields
}

const logger = 'winston logger configured with a logger, to register a use case execution'

module.exports = useCaseLogger(useCase, options, logger)
```
