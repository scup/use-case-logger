const dependencies = {
  lodashGet: require('lodash.get'),
  currentTime: Date.now
}

const STARTING_USE_CASE = 'STARTING_USE_CASE'
const USE_CASE_EXECUTED = 'USE_CASE_EXECUTED'
const USE_CASE_WITH_ERROR = 'USE_CASE_WITH_ERROR'
const USE_CASE_EXCEPTION = 'USE_CASE_EXCEPTION'
const USE_CASE_EXECUTING = 'USE_CASE_EXECUTING'

function reduceUseCaseParameters (parameters, value, index) {
  if (value && typeof value === 'object') {
    Object.assign(parameters, value)
    return parameters
  }

  parameters[`parameter${index}`] = value
  return parameters
}

function removeProperty (propertyName) {
  delete this.parametersToLog[propertyName]
}

class UseCaseExecutionLogger {
  constructor (useCase, injection) {
    const { logger } = Object.assign({}, dependencies, injection)

    Object.assign(this, {
      error: this.callLogger.bind(this, 'error'),
      warn: this.callLogger.bind(this, 'warn'),
      info: this.callLogger.bind(this, 'info'),
      verbose: this.callLogger.bind(this, 'verbose'),
      debug: this.callLogger.bind(this, 'debug'),
      silly: this.callLogger.bind(this, 'silly'),
      innerLogger: logger,
      metadata: { eventPhase: USE_CASE_EXECUTING, useCase: useCase.name }
    })
  }

  callLogger (level, message, contents) {
    this.innerLogger[level](message, Object.assign({}, contents, this.metadata))
  }
}

class UseCaseLogger {
  constructor (useCase, options, logger, injection) {
    const { currentTime, lodashGet } = Object.assign({}, dependencies, injection)
    Object.assign(this, {
      logger,
      currentTime,
      lodashGet,
      useCase,
      useCaseName: useCase.name,
      projectIdPath: Object.assign({}, options).projectIdPath,
      ignoreFields: Object.assign({}, options).ignoreFields,
      executeUseCase: this.executeUseCase.bind(this)
    })
  }

  registerUseCaseStart () {
    this.logger.debug(null, { eventPhase: STARTING_USE_CASE, useCase: this.useCaseName })
  }

  registerUseCaseEnd (initialTime, useCaseParameters, useCaseResult) {
    const now = this.currentTime()
    const projectId = this.lodashGet(useCaseParameters, this.projectIdPath)

    this.logger.info(null, {
      eventPhase: USE_CASE_EXECUTED,
      useCase: this.useCaseName,
      executionTime: now - initialTime,
      projectId,
      useCaseParameters: this.prepareParametersForLog(useCaseParameters)
    })
    return useCaseResult
  }

  registerUseCaseException (useCaseParameters, exception) {
    this.logger.info(null, {
      eventPhase: USE_CASE_WITH_ERROR,
      useCase: this.useCaseName,
      errorMessage: exception.message,
      useCaseParameters: this.prepareParametersForLog(useCaseParameters)
    })
    this.logger.debug(null, {
      eventPhase: USE_CASE_EXCEPTION,
      useCase: this.useCaseName,
      exception,
      useCaseParameters: this.prepareParametersForLog(useCaseParameters)
    })
    throw exception
  }

  prepareParametersForLog (useCaseParameters) {
    if (!Array.isArray(useCaseParameters)) {
      return useCaseParameters
    }

    const parametersToLog = useCaseParameters.reduce(reduceUseCaseParameters, {})
    if (Array.isArray(this.ignoreFields)) {
      this.ignoreFields.forEach(removeProperty, { parametersToLog })
    }

    return parametersToLog
  }

  executeUseCase () {
    const useCaseParameters = Array.from(arguments)

    try {
      this.registerUseCaseStart()
      const now = this.currentTime()
      const useCaseResult = this.useCase.apply(null, useCaseParameters)

      if (useCaseResult && useCaseResult.then && typeof useCaseResult.then === 'function') {
        return useCaseResult
          .then(this.registerUseCaseEnd.bind(this, now, useCaseParameters))
          .catch(this.registerUseCaseException.bind(this, useCaseParameters))
      } else {
        this.registerUseCaseEnd(now, useCaseParameters, useCaseResult)
      }

      return useCaseResult
    } catch (error) {
      this.registerUseCaseException(useCaseParameters, error)
    }
  }
}

function useCaseLogger (useCase, options, logger, injection) {
  const useCaseLoggerInstance = new UseCaseLogger(useCase, options, logger, injection)
  return useCaseLoggerInstance.executeUseCase
}

function useCaseExecutionLogger (useCase, injection) {
  return new UseCaseExecutionLogger(useCase, injection)
}

useCaseLogger.useCaseExecutionLogger = useCaseExecutionLogger
useCaseLogger.USE_CASE_WITH_ERROR = USE_CASE_WITH_ERROR

module.exports = useCaseLogger
