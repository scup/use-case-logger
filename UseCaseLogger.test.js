describe('useCaseLogger', () => {
  const { expect } = require('chai')
  const { mock, spy, stub, assert } = require('sinon')

  context('when usecase does not return promise', () => {
    context('and execution occurs WITHOUT exception', () => {
      let result
      let logger

      before(() => {
        const useCase = mock()
          .withExactArgs('1', 2, false, null, { value: 'any', ignore1: 'any', ignore2: 'any' }, undefined)
          .returns('any non promise result')

        logger = {
          info: spy(),
          debug: spy()
        }

        const currentTime = stub()
          .onFirstCall().returns(10)
          .onSecondCall().returns(13)

        const dependencies = { logger, currentTime }

        const options = {
          projectIdPath: '[1]',
          ignoreFields: ['ignore1', 'ignore2']
        }
        const useCaseComposed = require('./UseCaseLogger')(useCase, options, logger, dependencies)

        result = useCaseComposed('1', 2, false, null, { value: 'any', ignore1: 'any', ignore2: 'any' }, undefined)
      })

      it('returns the result of use case', () => expect(result).to.equal('any non promise result'))
      it('logs as debug use case start and all parameters', () =>
        assert.calledWithExactly(
          logger.debug,
          null, {
            eventPhase: 'STARTING_USE_CASE',
            useCase: 'proxy'
          }
        )
      )

      it('logs as info use case end with execution time and projectId = 2', () =>
        assert.calledWithExactly(
          logger.info,
          null,
          {
            eventPhase: 'USE_CASE_EXECUTED',
            executionTime: 3,
            projectId: 2,
            useCase: 'proxy',
            useCaseParameters: {
              parameter0: '1',
              parameter1: 2,
              parameter2: false,
              parameter3: null,
              parameter5: undefined,
              value: 'any'
            }
          }
        )
      )
    })

    context('and execution occurs WITH exception', () => {
      let error
      let logger

      before(() => {
        const useCase = mock().withExactArgs('1', {}).throws(new Error('The useCase error'))
        logger = {
          info: spy(),
          debug: spy()
        }

        const currentTime = stub()

        const dependencies = { logger, currentTime }

        const useCaseComposed = require('./UseCaseLogger')(useCase, { projectIdPath: '[1]' }, logger, dependencies)

        try {
          useCaseComposed('1', {})
          throw new Error('UseCaseLogger did not send the exception')
        } catch (errorThrown) {
          error = errorThrown
        }
      })

      it('throws the error of use case', () => expect(error.message).to.equal('The useCase error'))

      it('logs as info use case has error', () =>
        assert.calledWithExactly(
          logger.info,
          null,
          { useCase: 'proxy', eventPhase: 'USE_CASE_WITH_ERROR', errorMessage: 'The useCase error', useCaseParameters: { parameter0: '1' } }
        )
      )

      it('logs exception in debug level', () =>
        assert.calledWithExactly(
          logger.debug,
          null, {
            eventPhase: 'USE_CASE_EXCEPTION',
            exception: error,
            useCase: 'proxy',
            useCaseParameters: { parameter0: '1' }
          }
        )
      )
    })
  })

  context('when usecase returns promise', () => {
    context('and execution occurs WITHOUT exception', () => {
      let result
      let logger

      before(async () => {
        const useCase = mock().withExactArgs('1', 2, false, null, { value: 'any' }, undefined).resolves('any non promise result')
        logger = {
          info: spy(),
          debug: spy()
        }

        const currentTime = stub()
          .onFirstCall().returns(10)
          .onSecondCall().returns(13)

        const dependencies = { logger, currentTime }

        const useCaseComposed = require('./UseCaseLogger')(useCase, { projectIdPath: '[1]' }, logger, dependencies)

        result = await useCaseComposed('1', 2, false, null, { value: 'any' }, undefined)
      })

      it('returns the result of use case', () => expect(result).to.equal('any non promise result'))
      it('logs as debug use case start and all parameters', () =>
        assert.calledWithExactly(
          logger.debug,
          null, {
            eventPhase: 'STARTING_USE_CASE',
            useCase: 'proxy'
          }
        )
      )

      it('logs as info use case end with execution time and projectId = 2', () =>
        assert.calledWithExactly(
          logger.info,
          null,
          {
            eventPhase: 'USE_CASE_EXECUTED',
            executionTime: 3,
            projectId: 2,
            useCase: 'proxy',
            useCaseParameters: {
              parameter0: '1',
              parameter1: 2,
              parameter2: false,
              parameter3: null,
              parameter5: undefined,
              value: 'any'
            }
          }
        )
      )
    })

    context('and execution occurs WITH exception', () => {
      let error
      let logger

      before(async () => {
        const useCase = mock().withExactArgs('1', {}).rejects(new Error('The useCase error'))
        logger = {
          info: spy(),
          debug: spy()
        }

        const currentTime = stub()

        const dependencies = { logger, currentTime }

        const useCaseComposed = require('./UseCaseLogger')(useCase, { projectIdPath: '[1]' }, logger, dependencies)

        try {
          await useCaseComposed('1', {})
          throw new Error('UseCaseLogger did not send the exception')
        } catch (errorThrown) {
          error = errorThrown
        }
      })

      it('throws the error of use case', () => expect(error.message).to.equal('The useCase error'))

      it('logs as info use case has error', () =>
        assert.calledWithExactly(
          logger.info,
          null,
          { useCase: 'proxy', eventPhase: 'USE_CASE_WITH_ERROR', errorMessage: 'The useCase error', useCaseParameters: { parameter0: '1' } }
        )
      )

      it('logs as debug use case start and parameters without last', () =>
        assert.calledWithExactly(
          logger.debug,
          null, {
            eventPhase: 'USE_CASE_EXCEPTION',
            exception: error,
            useCase: 'proxy',
            useCaseParameters: { parameter0: '1' }
          }
        )
      )
    })
  })
})
